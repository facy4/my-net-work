import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementPlus from "element-plus"
import plugins from 'mytools/js/plugin'
import 'mytools/element-theme-chalk/src/index.scss'
let app=createApp(App);
app.use(plugins);
app.use(store).use(ElementPlus).use(router).mount('#app');
