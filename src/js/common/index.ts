import Http from "mytools/js/http";
export async function getRole() {
  let res = await Http.get("/query/userinfo", true);
  return res.role;
}
export const monthMap:any = {
   "Jan":0,
   "Feb":1,
   "Mar":2,
   "Apr":3,
   "May":4,
   "Jun":5,
   "Jul":6,
   "Aug":7,
   "Sept":8,
   "Oct":9,
   "Nov":10,
   "Dec":11,
};
export const moneyType:any = {
  "收入":'income',
  "支出":'expend',
};
const roleType:any = {
  "SM":'超级管理员',
  "M":'管理员',
  "V":'高级游客',
  "Y":'低级游客'
};
const gendList:any = {
  "1":'男',
  "2":'女',
  "3":'女装大佬',
  "4":'仆少女',
};
export const gendListFilter=(val:string)=>gendList[val];
export const roleListFilter=(val:string)=>roleType[val];