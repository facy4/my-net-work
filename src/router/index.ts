import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";
// import Login from "../views/baseicalViews/Login.vue";
// import Welcome from "../views/baseicalViews/welcome.vue";
// import Accumulation from "../views/accumulation/index.vue";
// import Manage from "../views/manage/index.vue";
// import Novel from "../views/novel/index.vue";
// import Utilities from "../views/utilities/index.vue";
// import personalDetail from "../views/baseicalViews/personalDetail.vue";
// import Home from "../views/Home.vue";
// import Test from "../views/baseicalViews/test.vue";
// import view_404 from "../views/baseicalViews/view_404.vue";
const Login = () => import(/* webpackChunkName: 'login' */ '../views/baseicalViews/Login.vue')
const Welcome = () => import(/* webpackChunkName: 'welcome' */ '../views/baseicalViews/welcome.vue')
const Accumulation = () => import(/* webpackChunkName: 'accumulation' */ '../views/accumulation/index.vue')
const Manage = () => import(/* webpackChunkName: 'manage' */ '../views/manage/index.vue')
const Novel = () => import(/* webpackChunkName: 'novel' */ '../views/novel/index.vue')
const Utilities = () => import(/* webpackChunkName: 'utilities' */ '../views/utilities/index.vue')
const personalDetail = () => import(/* webpackChunkName: 'personalDetail' */ '../views/baseicalViews/personalDetail.vue')
const Home = () => import(/* webpackChunkName: 'home' */ '../views/Home.vue')
const Test = () => import(/* webpackChunkName: 'test' */ '../views/baseicalViews/test.vue')
const view_404 = () => import(/* webpackChunkName: 'view_404' */ '../views/baseicalViews/view_404.vue')
import { getRole } from "../js/common/index";
import { ElMessage } from "element-plus";
const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "Login",
    component: Login,
  },
  {
    path: "/home",
    name: "Home",
    component: Home,
    redirect: "/home/welcome",
    children: [
      {
        path: "welcome",
        name: "Welcome",
        component: Welcome,
      },
      {
        path: "personaldetail",
        name: "PrsonalDetail",
        component: personalDetail,
      },
      {
        path: "manage",
        name: "Manage",
        meta: {
          role: ["M","SM"],
        },
        component: Manage,
      },
      {
        path: "accumulation",
        name: "Accumulation",
        component: Accumulation,
      },
      {
        path: "novel",
        name: "Novel",
        component: Novel,
      },
      {
        path: "utilities",
        name: "utilities",
        component: Utilities,
      },
    ],
  },
  {
    path: "/test",
    name: "test",
    component: Test,
  },
  {
    path: "/:catchAll(.*)",
    name: "404",
    component: view_404,
  },
];
function addRole(arr: Array<any>) {
  arr.forEach((item) => {
    if (item.name == "404") {
      return;
    }
    if (item.meta && item.meta.role.length > 0) {
    } else {
      item.meta = {};
      item.meta["role"] = ["Y"];
    }
    if (item.children) {
      addRole(item.children);
    }
  });
}
addRole(routes);
const router = createRouter({
  history: createWebHashHistory(),
  routes,
});
router.beforeEach(async (to: any, from) => {
  if (to.meta.role?.length>0) {
    if (to.name === "Login" || to.name === "register") {
      localStorage.clear();
      return true;
    }
    if (to.meta.role?.includes("D")) return true;
    if (!localStorage.getItem("token")) {
      ElMessage.error("请先登陆");
      return "/";
    }
    if (!to.meta.role?.includes("Y")) {
      let path = to.meta.role?.includes(await getRole()) ? true : "/";
      if (path === "/") {
        ElMessage.error("你权限不够");
      }
      return path;
    }
  }
});
export default router;
