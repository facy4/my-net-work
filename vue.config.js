const resolve = require("path")["resolve"];
const addAssetHtmlCdnWebpackPlugin = require("add-asset-html-cdn-webpack-plugin");
const htmlWebpackExternalsPlugin = require("html-webpack-externals-plugin");
const TerserPlugin = require('terser-webpack-plugin')
const CompressionPlugin = require('compression-webpack-plugin');
const cdns = require("mytools/js/cdn");
module.exports = {
  publicPath: "./",
  configureWebpack: (config) => {
    if(process.env.NODE_ENV=='production'){
      config.mode=process.env.NODE_ENV;
      config.plugins.push(
        new TerserPlugin({
          cache: true,
          parallel: true,
          sourceMap:false,
          terserOptions: {
            ecma: undefined,
            warning: false,
            parse: {},
            compress: {
              drop_console: true,
              drop_debugger: true,
              pure_funcs: [//移除console
                'console.log'
              ]
            }
          }
        })
      )
      config.plugins.push(new CompressionPlugin({
        algorithm: 'gzip', // 使用gzip压缩
        test: /\.js$|\.html$|\.css$/, // 匹配文件名
        filename: '[path].gz[query]', // 压缩后的文件名(保持原文件名，后缀加.gz)
        minRatio: 1, // 压缩率小于1才会压缩
        threshold: 10240, // 对超过10k的数据压缩
        deleteOriginalAssets: false, // 是否删除未压缩的源文件，谨慎设置，如果希望提供非gzip的资源，可不设置或者设置为false（比如删除打包后的gz后还可以加载到原始资源文件）
        })
      );
      config.plugins.push(
        new addAssetHtmlCdnWebpackPlugin(true, {
          vue: "https://unpkg.com/vue@3.0.5/dist/vue.global.prod.js",
          "element-plus-index":
            "https://unpkg.com/element-plus@1.0.2-beta.65/lib/index.full.js",
          "cropper-js":
            "https://cdn.bootcdn.net/ajax/libs/cropperjs/1.5.12/cropper.min.js",
          "cropper-css":
            "https://cdn.bootcdn.net/ajax/libs/cropperjs/1.5.12/cropper.min.css",
          "echarts":"https://cdn.jsdelivr.net/npm/echarts/dist/echarts.min.js",
        })
      );
      config.plugins.push(new htmlWebpackExternalsPlugin(cdns));
    }
  },
  css: {
    loaderOptions: {
      sass: {
        prependData: `
          @import "~mytools/element-my-theme/environment.scss";
        `,
      },
      postcss: {
        plugins: [
          require("postcss-px-to-viewport")({
            unitToConvert: "px", // (String) 需要转换的单位，默认为"px"
            viewportWidth: 1920, // (Number) 设计稿的视口宽度
            viewportHeight: 1560, // (Number) 设计稿的视口高度
            unitPrecision: 5, // (Number) 单位转换后保留的精度
            propList: ["*"], //  (Array) 能转化为vw的属性列表
            viewportUnit: "vw", //  (String) 希望使用的视口单位
            fontViewportUnit: "vw", // (String) 字体使用的视口单位
            selectorBlackList: [], // (Array) 需要忽略的CSS选择器，不会转为视口单位，使用原有的px等单位。
            minPixelValue: 1, // (Number) 设置最小的转换数值，如果为1的话，只有大于1的值会被转换
            mediaQuery: false, //  (Boolean) 媒体查询里的单位是否需要转换单位
            replace: true, // (Boolean) 是否直接更换属性值，而不添加备用属性
            exclude: /(\/|\\)(node_modules)(\/|\\)/, //  (Array or Regexp) 忽略某些文件夹下的文件或特定文件，例如 'node_modules' 下的文件
            landscape: false, // (Boolean) 是否添加根据 landscapeWidth 生成的媒体查询条件 @media (orientation: landscape)
          }),
          // require("postcss-pxtorem")({
          //   rootValue: 192,//结果为：设计稿元素尺寸/16，比如元素宽320px,最终页面会换算成 20rem
          //   propList: ['*']
          // }),
         
        ],
      },
    },
  },
  chainWebpack: (config) => {
    if(process.env.NODE_ENV=='production'){
    config.set("externals", {
      vue: "Vue",
      "element-plus": "ElementPlus",
      "crypto-js": "CryptoJS",
      "cropperjs": "Cropper",
      "echarts": "echarts"
    });
    }
   
    config.resolve.alias.set("@", resolve("src"));
  },
  devServer: {
    port: "8080",
    proxy: {
      "/api": {
        // target: "http://192.168.0.114:9999", // 后台接口域名
        target: "http://localhost:9999", // 后台接口域名
        changeOrigin: true, //是否跨域
        pathRewrite: {
          "^/api": "",
        },
      },
    },
  },
  productionSourceMap: false,
};
